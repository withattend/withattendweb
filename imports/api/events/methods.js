import { Meteor } from 'meteor/meteor';
import { SimpleSchema } from 'meteor/aldeed:simple-schema'
import { check } from 'meteor/check';
import phone from 'phone';

import { Events } from './events.js';
import { Users } from '../users/users.js';
import { Sites } from '../sites/sites.js';
import { JobCodes } from '../jobCodes/jobCodes.js';

Meteor.methods({
	'events.checkInDevice'(doc) {
		// Make sure the user is logged in before inserting an event
		if (! Meteor.userId()) {
    	throw new Meteor.Error("not-authorized");
    }
		
		//Build doc to test
		console.log(doc);
		doc.eventType = 1;
		doc.time = Date.create(doc.time+" GMT-0700 (PDT)");
		doc.createdAt = Date.create(doc.time);
		doc.foundRelated = false;
				
		//Check the data
		check(doc, Events.simpleSchema());

		//Submit the data
		Events.insert(doc, function(error,results){
			if(results) {
				console.log("device check in success");
			} else {
				throw new Meteor.Error("Device check in failed");
			}
		});
	},
	'events.checkOutDevice'(doc) {
		// Make sure the user is logged in before inserting an event
		if (! Meteor.userId()) {
    	throw new Meteor.Error("not-authorized");
    }
		
		//Build doc to test
		console.log(doc);
		doc.eventType = 0;
		doc.time = Date.create(doc.time+" GMT-0700 (PDT)");
		doc.createdAt = Date.create(doc.time);
		
		//Find check-in ID
		var checkIn = Events.findOne({userId: doc.userId, eventType: 1, beaconMajor: doc.beaconMajor, companyId: doc.companyId, foundRelated: false});
		
		if (checkIn) {
			doc.relatedId = checkIn._id;
			doc.foundRelated = true;
			Events.update({id:  checkIn._id}, { $set: { foundRelated : true, editor: Meteor.userId() } }, function(error,results){
				if(results) {
					console.log('Set event.foundRelated to true', results);
				} else {
					throw new Meteor.Error("Couldn't update object");
				}
			});
		}
		else {
			throw new Meteor.Error("Can't find related check-in");
		}
		
				
		//Check the data
		check(doc, Events.simpleSchema());
    
    //Submit the data
    Events.insert(doc, function(error,results){
	    if(results) {
		  	console.log("device check out success");
		  } else {
			   throw new Meteor.Error("Device check out failed");
			}
    });
	},
	'events.checkInNumber'(doc, phoneNumber) {
		// Make sure the user is logged in before inserting an event
		//TODO figure out the best way of securing this
		if (! Meteor.userId()) {
    		throw new Meteor.Error("not-authorized");
    	}
		
		//Lookup number
		var number = phone(phoneNumber, 'CAN');
		if (!_.isEmpty(number)) {
			
			//See if number exists
			const user = Users.findOne({ username: { $in: [number[0], phoneNumber]} },{ fields: { _id:1 } });
			
			if (user) {

				//Only check in user if they aren't on site already
				var onsite = Meteor.call('events.isUserOnSite', user._id);

				if (onsite) {
					throw new Meteor.Error("User on site already.");
				} else {
					doc.userId = user._id;
    		}

			} else {
				throw new Meteor.Error("No user with this number");
			}

		} else {

			//If not a valid #, still check
			const user = Users.findOne({ username: phoneNumber },{ fields: { _id:1 } });
			if (user) {

				//Only check in user if they aren't on site already
				var onsite = Meteor.call('events.isUserOnSite', user._id);

				if (onsite) {
					throw new Meteor.Error("User on site already.");
				} else {
					doc.userId = user._id;
    		}

			} else {
				throw new Meteor.Error("No user with this number");
			}

		}
		
		//Build doc to test
		doc.beaconMinor = '0';
		doc.eventType = 1;
		doc.editor = Meteor.userId();

		console.log(doc.time);
		console.log(Date.create());
		if (doc.time > Date.create()) {
			throw new Meteor.Error("Can't check-in in the future.");
		}
		
				
		//Check the data
		check(doc, Events.simpleSchema());
    
    //Submit the data
    Events.insert(doc, function(error,results){
	    //Print results or error
	    if(results) {
		  	console.log('Check in with phone number success ' + results);
		  } else {
			   console.log('Check in with phone number failed ' + error);
			}
    });
	},
	'events.checkInManual'(doc) {
		// Make sure the user is logged in before inserting an event
		//TODO figure out the best way of securing this
		if (! Meteor.userId()) {
    		throw new Meteor.Error("not-authorized");
    	}
		
		//Build doc to test
		doc.beaconMinor = '0';
		doc.eventType = 1;
		doc.editor = Meteor.userId();
        doc.foundRelated = false;
				
		//Check the data
		check(doc, Events.simpleSchema());
    
    	//Submit the data
    	Events.insert(doc, function(error,results){
	    	//Print results or error
	    	if(results) {
		  		console.log('manual check in success ' + results);
		  	} else {
			   console.log('manual check in failed ' + error);
			}
		});
	},
	'events.checkOutManual'(doc) {
		// Make sure the user is logged in before inserting an event
		//TODO figure out the best way of securing this
		if (! Meteor.userId()) {
    	throw new Meteor.Error("not-authorized");
    }
		
		//Build doc to test
		doc.beaconMinor = '0';
		doc.eventType = 0;
		doc.editor = Meteor.userId();
		
		//Find check-in ID
		var checkIn = Events.findOne({userId: doc.userId, eventType: 1, beaconMajor: doc.beaconMajor, companyId: doc.companyId, foundRelated: false});

		if (checkIn) {
			doc.relatedId = checkIn._id;
			doc.foundRelated = true;
			Events.update({id:  checkIn._id}, { $set: { foundRelated : true, editor: Meteor.userId() } }, function(error,results){
				if(results) {
					console.log('Set event.foundRelated to true', results);
				} else {
					throw new Meteor.Error("Couldn't update object");
				}
			});
		}
		else {
			throw new Meteor.Error("Can't find related check-in");
		}

		//Check the data
		check(doc, Events.simpleSchema());
    
    //Submit the data
    Events.insert(doc, function(error,results){
	    //Print results or error
	    if(results) {
		  	console.log('check out success ' + results);
		  } else {
			   console.log('check out failed ' + error);
			}
    });
	},
	'events.changeTime'(eventId, newTime) {
		// Make sure the user is logged in before inserting an event
		//TODO figure out the best way of securing this
		if (! Meteor.userId()) {
    		throw new Meteor.Error("not-authorized");
    	}

		//Get original time from event
		console.log(eventId);
		console.log(newTime);
		console.log(Date.create().beginningOfDay());
		var event = Events.findOne({ _id: eventId });
		if(eventId == false){
			//get user ID from somewhere.
			var doc = {};
			doc.beaconMinor = '0';
			doc.eventType = 0;
			doc.editor = Meteor.userId();
			doc.eventType = 0;
			doc.time = Date.create(newTime+" GMT-0700 (PDT)");
			doc.createdAt = Date.create(doc.time);
			doc.userId = startEvent;
			var start = new Date(newTime);
			start.setHours(0,0,0,0);
			var end = new Date(newTime);
			console.log(start);
			console.log(end);
			var checkIn = Events.findOne({ userId: doc.userId, eventType: 1, beaconMajor: doc.beaconMajor, companyId: doc.companyId, time: {$gte: start, $lt: end}});
			Events.insert(doc, function(error,results){
				if(results) {
					console.log("device check out success");
				} else {
					throw new Meteor.Error("Device check out failed");
				}
			});
		}
		else if(event.time) {
			//Add new time to existing date
			var newHour = Date.create(newTime).format('{HH}');
			var newMinute = Date.create(newTime).format('{mm}');
			var newDateTime = Date.create(event.time).set({ hour: newHour, minute: newMinute });
					
			//Make sure check-in isn't before check-out
			if (event.eventType === 0) {
				var eventOn = Events.findOne({ _id: event.relatedId });
				if (eventOn) {
					if (Date.create(eventOn.time).isAfter(newDateTime)) {
						throw new Meteor.Error("check-out must be after check-in");
					}
				}
			} else {	
				var eventOff = Events.findOne({ relatedId: event._id });
				if (eventOff) {
					if (Date.create(eventOff.time).isBefore(newDateTime)) {
						throw new Meteor.Error("check-out must be after check-in");
					}
				}
			}

	    //Submit the data & add editor
	    Events.update({ _id: eventId }, { $set: { time: newDateTime, editor: Meteor.userId() } }, function(error,results){
		    if(results) {
			  	console.log('Updated time ', results);
			  } else {
				  throw new Meteor.Error("Couldn't update time");
				}
	    });
		} else {
			throw new Meteor.Error("Invalid event.");
		}
	},
	'events.addJobCode'(eventId, jobCodeId) {
		// Make sure the user is logged in before editing an event
		//TODO figure out the best way of securing this
		if (! Meteor.userId()) {
    	throw new Meteor.Error("not-authorized");
    }
		
		//TODO - Check the data
		//check(doc, Events.simpleSchema());
    
    //Submit the data
    Events.update({ _id: eventId }, { $set: { jobCodeId: [jobCodeId] } }, function(error,results){
	    if(results) {
		  	console.log('Job code added to event ', results);
		} else {
			  throw new Meteor.Error("Couldn't add job code");
		}
    });
	},
	'events.isUserOnSite'(doc) {
		// Make sure the user is logged in before accessing this method
		if (! Meteor.userId()) {
    		throw new Meteor.Error("not-authorized");
    	}

		//Check if user was on site today
		var start = Date.create().beginningOfDay();
		var end = Date.create().endOfDay();
    	var events = Events.find({ userId: doc.userId, time: {$gte: start, $lt: end}, eventType: 1 }, { sort:{ time:-1 } }).fetch();
		var onsite = false;
		//If they were on site
  		if (!_.isEmpty(events)) {

    		var eventOff = Events.find({ $or: [{relatedId: events[0]._id},{userId:doc.userId, time: {$gte: events[0].time, $lt: end}, eventType:0}] },{fields:{time:1}, sort:{time:-1}}).fetch();

    		if (!_.isEmpty(eventOff)) {
	    		console.log("User already left site");
			} else {
	    		onsite = true;
			}
		} else {
    		console.log("User not on a site today");
		}
  		return onsite;

	},
	'events.appVersion'(doc) {
		if(doc.platform == "ios"){
			return 6;
		}
		if(doc.platform == "android"){
			return 15;
		}
	},
	'events.getReportData'(start, end, companyId) {
		// Make sure the user is logged in before accessing this method
		//TODO ensure they are a manager
		if (! Meteor.userId()) {
    		throw new Meteor.Error("not-authorized");
    	}

		var self = this;
		var data = [];

		//Get events
		var checkInEvents = Events.find({ companyId: companyId, time: {$gte: start, $lt: end}, eventType: 1 }).fetch();
		var checkOutEvents = Events.find({ companyId: companyId, time: {$gte: start, $lt: end}, eventType: 0 }).fetch();
		//Group by user
		let userEvents = _.groupBy(checkInEvents.sortBy(function(event){ return event.userId }), function(event) {
			return event.userId;
		});
		//Loop through each user
		_.each(userEvents, function(events, userId) {

			//Get total hours for user
			var userSum = 0;
			var date;
			//Get user name to display
			var currentUser = Users.findOne({ _id: userId });
			var userName = (currentUser && currentUser.profile) ? currentUser.profile.firstName + ' ' + currentUser.profile.lastName : "No Name";
			//Get events grouped by site
			let siteEvents = _.groupBy(events, function(event) {
				return event.beaconMajor;
			});
			console.log(userName);
			if(userName != "No Name")
			{
				//Loop through each site
				_.each(siteEvents, function (userSiteEvents, beaconMajor) {

					//Get site name to display
					var currentSite = Sites.findOne({beaconMajor: beaconMajor});
					var siteName = currentSite ? currentSite.name : "Old Site";
					//Get events grouped by code & loop
					let timeSortedEvents = userSiteEvents.sort(function (a, b) {
						a = new Date(a.time).getTime();
						b = new Date(b.time).getTime();
						return a - b
					});
					//if there are multiple events for given date check if times overlap and get total time worked for that day
					for (i in timeSortedEvents) {
						//move this for loop inside of lower .each as it is redundant and can be done at the same time as
						//getting the endEvent event
						var a = new Date(timeSortedEvents[i].time);
						for (j in timeSortedEvents) {
							var b = new Date(timeSortedEvents[j].time);
							if (i != j && a.getDate() == b.getDate() && a.getMonth() == b.getMonth() && a.getFullYear() == b.getFullYear()) {
								console.log(timeSortedEvents[i]._id);
								var endA = Events.find({
									$or: [{relatedId: timeSortedEvents[i]._id}, {
										userId: timeSortedEvents[i].userId,
										time: {$gte: timeSortedEvents[i].time, $lt: Date.create(timeSortedEvents[i].time).endOfDay()},
										eventType: 0,
										beaconMajor: timeSortedEvents[i].beaconMajor
									}]
								}, {fields: {time: 1}, sort: {time: 1}}).fetch();
								var endB = Events.find({
									$or: [{relatedId: timeSortedEvents[j]._id}, {
										userId: timeSortedEvents[j].userId,
										time: {$gte: timeSortedEvents[j].time, $lt: Date.create(timeSortedEvents[j].time).endOfDay()},
										eventType: 0,
										beaconMajor: timeSortedEvents[j].beaconMajor
									}]
								}, {fields: {time: 1}, sort: {time: 1}}).fetch();
								//rework this so the latest array element is used.
								if (endA.length > 0 && endB.length > 0) {
									var endADate = new Date(endA);
									var endBDate = new Date(endB);
									var endAIndex;
									var endBIndex;
									for (k in checkOutEvents) {
										if (endA[0]._id === checkOutEvents[k]._id) {
											endAIndex = k;
											break;
										}
									}
									for (k in checkOutEvents) {
										if (endB[0]._id === checkOutEvents[k]._id) {
											endBIndex = k;
											break;
										}
									}
									//events happened on same day now check to see what times they overlapped, take the earlest arival
									//time and latest exit time to be nice to the workers.
									//update earliest and lastest times then delete unneeded times
									if (a.getTime() < b.getTime()) {
										//entrytime a is earlier than b, so b is deleted
										if (endADate.getTime() < endBDate.getTime()) {
											//endtime a is earlier than endtime b so endtime a is deleted
											checkOutEvents[endBIndex].relatedId = timeSortedEvents[i]._id;
											checkOutEvents.splice(endAIndex, 1);
										} else {
											//endtime a is later than endtime b so endtime b is deleted
											checkOutEvents[endAIndex].relatedId = timeSortedEvents[i]._id;
											checkOutEvents.splice(endBIndex, 1);
										}
										timeSortedEvents.splice(j, 1);
									}
									else {
										//entrytime a is later than b, so a is deleted
										if (endADate.getTime() < endBDate.getTime()) {
											//endtime a is earlier than endtime b so endtime a is deleted
											checkOutEvents[endBIndex].relatedId = timeSortedEvents[j]._id;
											checkOutEvents.splice(endAIndex, 1);

										} else {
											//endtime a is later than endtime b so endtime b is deleted
											checkOutEvents[endAIndex].relatedId = timeSortedEvents[j]._id;
											checkOutEvents.splice(endBIndex, 1);
										}
										timeSortedEvents.splice(i, 1);
									}
								}
							}
						}
					}
					//Multiple CheckOutEvents are being created that point to the same check-in event
					_.each(timeSortedEvents, function (startEvent) {
						//standard time is 7 hours ahead. To convert back to BC time time is set back 7 hours.
						console.log(startEvent);
						var startEventTime = new Date(startEvent.time);
						startEventTime.setHours((startEventTime.getHours() - 7),startEventTime.getMinutes());
						var endEvent;
						for(i in checkOutEvents){
							var outEvent = checkOutEvents[i];
							if(outEvent.relatedId == startEvent._id) {
								endEvent = outEvent;
							}
						}
						if(endEvent == null || typeof endEvent === undefined){
							for(i in checkOutEvents){
								var outEvent = checkOutEvents[i];
								if(outEvent.userId == startEvent.userId && outEvent.time > startEvent.time && outEvent.time <
									Date.create(startEvent.time).endOfDay() && outEvent.beaconMajor == startEvent.beaconMajor) {
									endEvent = outEvent;
								}
							}
						}

						/*Events.find({
							$or: [{relatedId: startEvent._id}, {
								userId: startEvent.userId,
								time: {$gte: startEvent.time, $lt: Date.create(startEvent.time).endOfDay()},
								startEventType: 0,
								beaconMajor: startEvent.beaconMajor
							}]
						}, {fields: {time: 1}, sort: {time: 1}}).fetch()[0];*/
						//parse time to pacific time
						var endEventTime;
						if (!_.isEmpty(endEvent)) {
							endEventTime = new Date(endEvent.time);
							endEventTime.setHours((endEventTime.getHours() - 7),endEventTime.getMinutes());
						}
						var codeNumber = 'no code';
						var displayHours = "n/a";
						var jobCodeId = !_.isEmpty(startEvent.jobCodeId) ? startEvent.jobCodeId[0] : 'no code';
						if (jobCodeId != 'no code') {
							var currentCode = JobCodes.findOne({_id: startEvent.jobCodeId[0]});
							if (typeof currentCode !== "undefined") {
								if (currentCode.code != null) {
									codeNumber = currentCode.code;
								}
							}
						}
						//Try and get end time based on ID, otherwise use data from start time, and sort by most recent after start time
						//Displays end time and hours worked correctly
						var minutesWorked;
						if (!_.isEmpty(endEvent)) {
							//work starts at 7
							//times are off by one hour
							if (parseInt(startEventTime.getHours()) >= 7) {
								//arrived at work late
								console.log("late");
								minutesWorked = (endEventTime.getHours() - startEventTime.getHours())*60 + (endEventTime.getMinutes() - startEventTime.getMinutes());
							}
							else {
								//arrived at work early so setting time to 7
								console.log("early");
								minutesWorked = (endEventTime.getHours() - 7)*60 + endEventTime.getMinutes();
							}
						} else {
							minutesWorked = "n/a";
						}
						if (minutesWorked != "n/a") {
							console.log(minutesWorked);
							var roundedMinutes = 15*Math.round(minutesWorked/15);
							userSum += roundedMinutes;
							displayHours = (roundedMinutes > 0) ? getHours(roundedMinutes) : '0';
							console.log(displayHours);
							//displayHours = minutesWorked;
						}
						//get startEvent date
						//Friday, July 21, 7AM - 3PM
                        var dateString = Date.create(startEventTime).short()+ ", " + startEventTime.format("{h}:{mm} {tt}");
						if (!_.isEmpty(endEvent)) {
							dateString += " - " + endEventTime.format("{h}:{mm} {tt}");
						}
						else{
							dateString += " - End time not entered."
						}
						if (userName != "No Name") {
							var newData = {
								"Name": userName,
								"Site": siteName,
								"Code": codeNumber,
								"Hours worked": (minutesWorked < 480) ? displayHours.toUpperCase() : displayHours,
								"Date": dateString
							};
							data.push(newData);
						}
					});
				});
				//Print sum for each user, then a space
				var displayHoursUser = (userSum > 0) ? getHours(userSum) : '0';
				if (userName != "No Name") {
					var newData =
					{
						"Name": userName,
						"Site": '',
						"Code": 'Total',
						"Hours worked": displayHoursUser,
						"date": ''
					};
					data.push(newData);

					var dataSpace =
					{
						"Name": '',
						"Site": '',
						"Code": '',
						"Hours worked": '',
						"date": ''
					};
					data.push(dataSpace);
				}
			}

		});


		return data;

	}
});